<?php

/**
 * Implements hook_menu().
 */
function grabber_menu() {
    $items['grabber/test'] = array (
        'page callback' => 'grabber_test',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
        'file' => 'grabber.admin.php',
    );

    $items['grabber/test/loader'] = array (
        'page callback' => 'grabber_test_loader',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );

    $items['grabber/test/scanner'] = array (
        'page callback' => 'grabber_test_scanner',
        'access arguments' => array('access content'),
        'type' => MENU_CALLBACK,
    );

    $items['admin/config/administration/grabber'] = array(
        'title' => 'Grabber',
        'description' => 'Configure Grabber',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('grabber_settings_form'),
        'access arguments' => array('administer site configuration'),
        'file' => 'grabber.admin.php',
    );

    $items['admin/config/administration/grabber/loader'] = array(
        'title' => 'Loader',
        'description' => 'Configure Grabber Loader',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('grabber_loader_settings_form'),
        'access arguments' => array('administer site configuration'),
        'file' => 'grabber.admin.php',
    );

    $items['admin/config/administration/grabber/types'] = array(
        'title' => 'Types',
        'description' => 'Configure types',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('grabber_types_form'),
        'access arguments' => array('administer site configuration'),
        'file' => 'grabber.admin.php',
    );
    $items['admin/config/administration/grabber/type/%'] = array(
        'title' => 'Type',
        'description' => 'Configure type',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('grabber_type_form', 5),
        'access arguments' => array('administer site configuration'),
        'file' => 'grabber.admin.php',
    );
/*
    $items['admin/config/administration/grabber/scanners'] = array(
        'title' => 'Grabber',
        'description' => 'Configure Grabber',
        'page callback' => 'grabber_settings',
        'access arguments' => array('administer site configuration'),
        'file' => 'grabber.admin.php',
    );

    $items['admin/config/administration/grabber/scanners/%'] = array(
        'title' => 'Node type grabbers',
        'description' => 'Configure Grabber',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('grabber_concrete_settings_form', 4),
        'access arguments' => array('administer site configuration'),
        'file' => 'grabber.admin.php',
    );
*/
    $items['admin/reports/grabber'] = array(
        'title' => 'Grabber log',
        'description' => 'Show grabbing process log',
        'page callback' => 'grabber_report_log',
        'access arguments' => array('administer content types'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'grabber.admin.php',
    );

    $items['admin/reports/grabber/clear'] = array(
        'title' => 'Clear Grabber log',
        'description' => 'Clear',
        'page callback' => 'grabber_report_log_clear',
        'access arguments' => array('administer content types'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'grabber.admin.php',
    );

    return $items;
}

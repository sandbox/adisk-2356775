<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 20.09.14
 * Time: 21:16
 */

require_once('include/NodeHelper.php');
require_once('include/Coordinator.php');
require_once('include/Settings.php');
require_once('include/Store.php');

function grabber_settings() {
    // get grabbers
    // show list
    $coordinator = new \Grabber\Coordinator();
    $grabbers = $coordinator->get_grabbers();

    $header = array(t('Name'));
    $rows = array();

    foreach($grabbers as $grabber) {
        $grabber_name = get_class($grabber);
        $exploded = explode('\\', $grabber_name);
        $grabber_name = array_pop($exploded);
        $rows[] = array(l($grabber_name, "admin/config/administration/grabber/{$grabber_name}"));
    }

    $build['node_table'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No grabbers available. Check folder '.(__DIR__.'/grabbers')),
    );

    return $build;
}

function grabber_settings_form($form, &$form_state) {
    $form = array();
    $types = array();

    foreach(node_type_get_types() as $id=>$type) {
        $types[$id] = $type->name;
    }

    $form['map'] = array(
        '#type' => 'markup',
        '#markup' => grabber_settings_map(),
    );

    $form['splitter1'] = array(
        '#type' => 'markup',
        '#markup' => '<br>',
    );


    /* in grabber
    $form['grabber_node_type'] = array(
        '#title' => 'Node type',
        '#type' => 'select',
        '#options' => $types,
        '#default_value' => 'happy',
    );
    */

    $form['grabber_scan_interval'] = array(
        '#title' => 'Scan interval',
        '#type' => 'select',
        '#options' => array(
            '1' => 'каждый час',
            '2' => '12 раз в день',
            '3' => '8 раз в день',
            '4' => '6 раз в день',
            '6' => '4 раза в день',
            '12' => '2 раза в день',
            '24' => '1 раз в день',
        ),
        '#default_value' => '24',
    );

    $form['grabber_load_interval'] = array(
        '#title' => 'Load interval',
        '#type' => 'select',
        '#options' => array(
            '1' => 'каждый час',
            '2' => '12 раз в день',
            '3' => '8 раз в день',
            '4' => '6 раз в день',
            '6' => '4 раза в день',
            '12' => '2 раза в день',
            '24' => '1 раз в день',
        ),
        '#default_value' => '12',
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );

    return $form;
}

function grabber_settings_map() {
    $module_path = drupal_get_path('module', 'grabber');
    drupal_add_css($module_path . '/css/admin.css', 'file');

    $store = new \Grabber\Store();
    $grabbed = $store->get_grabbed();
    $grabbed_count = count($grabbed);

    $log_errors_count = \Grabber\Logger::get_errors_count();

    ob_start();
    ?>

    <div id="grabber-map">
        <div class="object" id="grabber-map-loader"> Loader
            <div class="menu">
                <div class="menu-item"><a href="/grabber/test/loader"> Запустить </a></div>
            </div>
        </div>
        <div class="object" id="grabber-map-scanner"> Scanner
            <div class="menu">
                <div class="menu-item"><a href="/grabber/test/scanner"> Запустить </a></div>
                    <!--
                    <select>
                        <option value="24">Каждый час</option>
                        <option value="24">Каждые 2 часа</option>
                        <option value="24">Раз в день</option>
                    </select>
                    -->
            </div>
        </div>
        <div class="object" id="grabber-map-logger"> Logger

            <?php if($log_errors_count): ?>
                <div class="counter counter-1 counter-error"> <?= $log_errors_count ?> </div>
            <?php endif; ?>

            <div class="menu">
                <div class="menu-item"><a href="/admin/reports/grabber"> Журнал </a></div>
                <div class="menu-item"><a href="/admin/reports/grabber/clear"> Очистить </a></div>
            </div>
        </div>
        <div class="object" id="grabber-map-store"> Store
            <div class="counter counter-1"> <?= $grabbed_count ?> </div>
        </div>
    </div>
    <hr>

    <?php
    $html = ob_get_contents();
    ob_end_clean();

    return $html;
}

function grabber_settings_form_submit($form, &$form_state) {
    //$grabber_node_type = $form_state['values']['grabber_node_type'];
    //variable_set('grabber_node_type', $grabber_node_type);

    $grabber_scan_interval = $form_state['values']['grabber_scan_interval'];
    variable_set('grabber_scan_interval', $grabber_scan_interval);

    $grabber_load_interval = $form_state['values']['grabber_load_interval'];
    variable_set('grabber_load_interval', $grabber_load_interval);
}
/*
function grabber_concrete_settings_form($vars, $vars2, $node_type, $grabber_name) {
    // load grabber
    //   get fields
    // load node type
    //   get fields
    // load mapping
    // render form

    drupal_add_css(drupal_get_path('module', 'grabber') . '/css/admin.css');

    $grabber_class = "\\Grabber\\{$grabber_name}";
    $grabber_fields = array('', 'field_place', 'category', 'city');

    $node_fields = \Grabber\NodeHelper::get_node_fields($node_type);

    $map = variable_get("grabber-map-{$grabber_name}", array());

    $form = array();

    $form['map'] = array('#type' => 'fieldset', '#title' => t('Mapping'));

    $form['map']['node_type'] = array(
        '#type' => 'hidden',
        '#value' => $node_type,
    );

    $form['map']['grabber_name'] = array(
        '#type' => 'hidden',
        '#value' => $grabber_name,
    );

    foreach($node_fields as $node_field) {
        $form['map'][$node_field] = array(
            '#title' => $node_field,
            '#type' => 'select',
            '#options' => array_combine($grabber_fields, $grabber_fields),
            '#default_value' => isset($map[$node_field]) ? $map[$node_field] : grabber_suggest_field($node_field, $grabber_fields),
            '#weight' => 0,
        );
    }

    $form['map']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );


    return $form;
}
*/

function grabber_suggest_field($field, $all_possible) {
    // 1. equal
    foreach($all_possible as $possible) {
        if ($field == $possible) {
            return $possible;
        }
    }

    // 2. by word
    foreach($all_possible as $possible) {
        $exploded = explode('_', $possible);

        if (in_array($field, $exploded)) {
            return $possible;
        }

        $exploded = explode('-', $possible);

        if (in_array($field, $exploded)) {
            return $possible;
        }

        // reverse
        $exploded = explode('_', $field);

        if (in_array($possible, $exploded)) {
            return $possible;
        }

        $exploded = explode('-', $field);

        if (in_array($possible, $exploded)) {
            return $possible;
        }

    }

    // 3. contains
    foreach($all_possible as $possible) {
        if ($field && $possible && stristr($field, $possible) !== FALSE) {
            return $possible;
        }

        //reverse
        if ($possible && $field && stristr($possible, $field) !== FALSE) {
            return $possible;
        }
    }

    // 4. none
    return  '';
}

/*
function grabber_concrete_settings_form_submit($form, &$form_state) {
    $node_type = $form_state['values']['node_type'];
    $grabber_name = $form_state['values']['grabber_name'];
    variable_set("grabber-map-{$node_type}-{$grabber_name}", $form_state['values']);
}
*/

function grabber_report_log() {
    $data = db_query('SELECT * FROM {grabber_log} ORDER BY id DESC');

    $header = array(t('Time'), t('Type'), t('Message'));
    $rows = array();

    foreach ($data as $dat) {
        $rows[]['data'] = array(
            date('Y-m-d H:i:s', $dat->created),
            $dat->type,
            $dat->message
        );
    }

    $output =
        '<div>' . l(t('Clear log'), 'admin/reports/grabber/clear') . '</div>'
        . theme('table', array('header' => $header, 'rows' => $rows, 'empty' => 'No log entries'));

    return $output;
}

function grabber_report_log_clear() {
    \Grabber\Logger::clear();
    drupal_goto('admin/config/administration/grabber');
}

function grabber_test() {
    //$entity = \Grabber\NodeHelper::find('happy', array('title' => 'Люси'));
    //var_dump($entity);
    $ec = entity_get_controller('field_collection_item');
    var_dump($ec);
    die();
}

function grabber_loader_settings_form($form, &$form_state) {
}

function grabber_loader_settings_form_submit($form, &$form_state) {
}

function grabber_types_form($form, &$form_state) {
    $types = node_type_get_types();
    ksort($types);
    $rows = array();
    $header = array(t('Тип Материала'));

    foreach($types as $id=>$type) {
        $rows[]['data'] = array(
            l($type->name, 'admin/config/administration/grabber/type/' . $id),
        );
    }

    $form['types'] = array(
        '#type' => 'markup',
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows, 'empty' => 'No fields')),
    );

    return $form;
}

function grabber_type_form($form, &$form_state, $node_type) {
    $form = array();
    $settings = \Grabber\Settings::grabber_get_type_settings($node_type);

    // get node fields
    $info = \Grabber\NodeHelper::get_node_info('node', $node_type);

    $header = array(t('Field'), t('Объединять'), t('Уникально'), t('Уник. по полю'));
    $rows = array();

    foreach ($info as $field_name=>$big_info) {
        $field_info = $big_info['field_info'];
        $field_type = $field_info['type'];
        // print_r($field_info['columns'], TRUE)

        if ($field_type == 'field_collection') {
            $fc_info = \Grabber\NodeHelper::get_node_info('field_collection_item', $field_name);

            $merge_checked = $settings[$field_name]['merge'] ? 'checked' : '';
            $unique_checked = $settings[$field_name]['unique'] ? 'checked' : '';
            $unique_field = $settings[$field_name]['unique_field'];

            // options
            $options = '';
            foreach($fc_info as $fc_field=>$fc_field_info) {
                $unique_field_selected = $unique_field == $fc_field ? 'selected' : '';
                $options .= '<option value="'.$fc_field.'" ' . $unique_field_selected . '>' . $fc_field . ' (' . $fc_field_info['label'] . ')' . '</option>';
            }

            $rows[]['data'] = array(
                $field_name . ' (' . $big_info['label'] . ')',
                '<input type="checkbox" name="merge['.$field_name.']" value="1" ' . $merge_checked . '>',
                '<input type="checkbox" name="unique['.$field_name.']" value="1" ' . $unique_checked . '>',
                '<select name="unique_field['.$field_name.']">'.$options.'</select>',
            );
        } else {
            $rows[]['data'] = array(
                $field_name . ' (' . $big_info['label'] . ')',
                '',
                '',
                '',
            );
        }
    }

    $output = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => 'No fields'));

    $form['node_type'] = array(
        '#type' => 'hidden',
        '#value' => $node_type,
    );

    $form['fields'] = array(
        '#type' => 'markup',
        '#markup' => $output,
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );

    return $form;
}

function grabber_type_form_submit($form, &$form_state) {
    $settings = array();
    $node_type = $_POST['node_type'];
    $info = \Grabber\NodeHelper::get_node_info('node', $node_type);

    foreach ($info as $field_name=>$big_info) {
        $settings[$field_name] = array(
            'merge' => isset($_POST['merge'][$field_name]) ? TRUE : FALSE,
            'unique' => isset($_POST['unique'][$field_name]) ? TRUE : FALSE,
            'unique_field' => isset($_POST['unique_field'][$field_name]) ? $_POST['unique_field'][$field_name] : '',
        );
    }

    variable_set('grabber_type_settings_' . $node_type, $settings);
};


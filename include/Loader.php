<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 12.09.14
 * Time: 11:59
 */

namespace Grabber;

require_once('Mapper.php');
require_once('Writer.php');
require_once('WriterFactory.php');
require_once('NodeHelper.php');
require_once('TaxonomyHelper.php');
require_once('Optimizer.php');
require_once('Store.php');
require_once('Logger.php');


/**
 * Class Loader
 * @package Grabber
 */
class Loader {
    function execute() {
        // load
        $store = new Store();
        $items = $store->get_grabbed();

        // optimize
        $optimizer = new Optimizer($store);
        $optimizer->execute($items);

        // process
        foreach($items as $item) {
            try {
                $this->process_item($item['item']);
                $store->backup($item);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }
        }
    }

    function process_item(array $item) {
        // lookup for exists
        $node_type = $item['node_type'];

        if (!$node_type) {
            throw new \Exception('Undefined node type.');
        }

        $taxonomy_helper = new TaxonomyHelper();

        $entity = \Grabber\NodeHelper::find(
            $node_type,
            array(
                'title' => $item['title'],
                'status' => 1,
                'field_city' => $taxonomy_helper->get_term('city', $item['field_city'], $add_missed=FALSE),
                'field_place' => $taxonomy_helper->get_term('place', $item['field_place'], $add_missed=FALSE),
                'field_categoryhappy' => $taxonomy_helper->get_term('category', $item['field_categoryhappy'], $add_missed=FALSE),
            )
        );

        // create
        if (!$entity) {
            $entity = \Grabber\NodeHelper::create($node_type, $item);
        }

        // TODO remap fields
        //$mapper = new Mapper();
        //$mapped_item = $mapper->execute($node_type, $grabber_name, $item);

        // set values
        $writer = new Writer();
        $writer->execute($entity, $item);

        // save
        NodeHelper::save($entity);
    }
}

// TODO check for already loaded poster. not load dup
// TODO check for already filled info. skip. mark modified
// TODO don't save not modified node
// TODO многопоточность сканнирования
// TODO если не удается получить изображение постера - вернуть старое
// TODO cache node

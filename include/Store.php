<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 13.09.14
 * Time: 11:28
 */

namespace Grabber;

require_once('Logger.php');

/**
 * Class Store
 * @package Grabber
 */
class Store {
    const BACKUP_FOLDER = 'ok';
    var $path = '/tmp/grabber';

    function __construct() {
        $uri = 'public://grabber';
        if ($wrapper = file_stream_wrapper_get_instance_by_uri($uri)) {
            $this->path = $wrapper->realpath();
        }
    }

    /**
     * @param $dir
     */
    function make_dir($dir) {
        if (!file_exists($dir)) {
            mkdir($dir, 0777, TRUE);
        }
    }

    /**
     * @param $filename
     * @param $content
     * @return mixed
     */
    function write(array $item) {
        $grabber = $item['grabber'];

        if (!isset($item['item_folder'])) {
            $timestamp = microtime(true);
            $filename = "{$grabber}/{$timestamp}/parsed.json";
            $file = $this->path . '/' . $filename;
        } else {
            $file = $item['item_folder'] . '/parsed.json';
        }

        $dir = dirname($file);
        $this->make_dir($dir);

        $item['item_folder'] = $dir;
        $content = json_encode($item);

        file_put_contents($file, $content);

        return $file;
    }

    function scandir_through($dir)
    {
        $items = glob($dir . '/*');

        for ($i = 0; $i < count($items); $i++) {
            if (is_dir($items[$i])) {
                if (basename($items[$i]) == self::BACKUP_FOLDER) {
                    continue;
                }
                $add = glob($items[$i] . '/*');
                $items = array_merge($items, $add);
            }
        }

        return $items;
    }

    function get_grabbed() {
        $dir = $this->path;
        $all = $this->scandir_through($dir);
        $items = array();

        // filter
        $filtered = array();

        foreach($all as $file) {
            if (substr($file, -11) == 'parsed.json') {
                $filtered[] = $file;
            }
        }

        // load
        foreach($filtered as $file) {
            $item_folder = dirname($file);

            // item
            try {
                $item = $this->load_item($file);
            } catch (\Exception $e) {
                continue;
            }

            // merge fetched
            $fetched_file = $item_folder . '/' . 'fetched.json';
            if (file_exists($fetched_file)) {
                try {
                    $fetched = $this->load_item($fetched_file);
                    $item = array_merge($item, $fetched);
                } catch (\Exception $e) {
                    // no fetched data
                    Logger::error($e->getMessage());
                }
            }

            $item['item_folder'] = $item_folder;
            $items[] = $item;
        }

        return $items;
    }

    function load_item($item_file) {
        if (!file_exists($item_file))
            throw new \Exception("No file exists: {$item_file}");

        $json = file_get_contents($item_file);
        $item = json_decode($json, TRUE);

        return $item;
    }

    function backup($item) {
        $folder = $item['item_folder'];
        $destination = $folder . '/../' . self::BACKUP_FOLDER . '/' . basename($folder);
        $this->make_dir($destination);
        rename($folder, $destination);

    }

    function write_image($filename, $content) {
        //
    }
}

// TODO clear old backup folders

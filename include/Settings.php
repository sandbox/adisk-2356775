<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 11.10.14
 * Time: 17:17
 */

namespace Grabber;

require_once('NodeHelper.php');

class Settings {
    static function grabber_get_type_settings($node_type) {
        $settings = array();

        $info = \Grabber\NodeHelper::get_node_info('node', $node_type);

        // defaults
        foreach ($info as $field_name=>$big_info) {
            $settings[$field_name] = array(
                'merge' => FALSE,
                'unique' => FALSE,
                'unique_field' => '',
            );
        }

        // configured
        $saved = variable_get('grabber_type_settings_' . $node_type, array());

        foreach ($settings as $field_name=>&$values) {
            if (isset($saved[$field_name])) {
                $values = array_merge($values, $saved[$field_name]);
            }
        }

        return $settings;
    }
}


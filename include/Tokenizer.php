<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 13.09.14
 * Time: 11:56
 */

namespace Grabber;


/**
 * Class Tokenizer
 * @package Grabber
 */
class Tokenizer {
    var $tokens = array();

    /**
     * @param array $tokens
     * @return $this
     */
    function set_tokens(array $tokens) {
        $this->tokens = $tokens;
        return $this;
    }

    /**
     * @param $pattern
     * @param array $tokens
     * @return mixed
     */
    function execute($pattern, array $tokens=array()) {
        $result = $pattern;

        $merged = array_merge($this->tokens, $tokens);

        foreach($merged as $token=>$value) {
            $result = str_replace('[' . $token . ']', $value, $result);
        }

        return $result;
    }
} 
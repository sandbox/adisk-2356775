<?php

namespace Grabber;

require_once('WriterFactory.php');


class NodeHelper
{
    static function find($node_type='', array $conditions=array()) {
        $info = self::get_node_info('node', $node_type);

        $query = new \EntityFieldQuery();
        $query->entityCondition('entity_type', 'node');

        if ($node_type)
            $query->propertyCondition('type', $node_type);

        foreach($conditions as $field=>$value) {
            if (($field == 'title' || $field == 'status')) {
                if (!is_null($value)) {
                    $query->propertyCondition($field, $value);
                }
                continue;
            }

            $property = self::get_central_property($info, $field);
            $query->fieldCondition($field, $property, $value);
        }

        $entities = $query
            ->range(0,1)
            ->execute();

        if (!empty($entities['node'])) {
            $one = array_shift($entities['node']);
            $node = node_load($one->nid);
            return $node;
        }

        return FALSE;
    }

    /**
     * @param string $node_type
     * @param array $data
     * @return bool
     */
    static function create($node_type, array $data=array()) {
        global $user;

        $entity = entity_create('node', array(
                'type' => $node_type,
                'uid' => $user->uid,
                'title' => $data['title'],
            ));

        node_object_prepare($entity);

        return $entity;
    }

    static function get_central_property($info, $field_name) {
        $field_info = $info[$field_name]['field_info'];
        $field_type = $field_info['type'];
        $settings = $info[$field_name];

        $map = array(
            'datetime' =>                'value',
            'date' =>                    'value',
            'datestamp' =>               'value',
            'taxonomy_term_reference' => 'tid',
            'field_collection' =>        'value',
            'text' =>                    'value',
            'text_long' =>               'value',
            'text_with_summary' =>       'value',
            'number_integer' =>          'value',
            'image' =>                   'fid',
            'media' =>                   'fid',
            'file' =>                   ' fid',
        );

        if (!isset($map[$field_type])) {
            throw new \Exception("Unsupported field type: {$field_type}");
        }

        return $map[$field_type];
    }

    static function get_node_info($entity_type, $bundle_name) {
        $info = Cache::get("node-info-{$entity_type}");

        if ($info) {
            return $info;
        }

        $info = field_info_instances($entity_type, $bundle_name);

        //$field_types = field_info_field_types();
        //$widget_types = field_info_widget_types();
        //$extra_fields = field_info_extra_fields($entity_type, $bundle, 'form');

        foreach($info as $field_name=>&$instance) {
            $instance['field_info'] = field_info_field($instance['field_name']);
        }

        Cache::set("node-info-{$entity_type}", $info);

        return $info;
    }

    static function get_node_fields($node_type) {
        $info = self::get_node_info('node', $node_type);
        return array_keys($info);
    }

    static function get_reserved_fields() {
        return array('title', 'status', 'sticky', 'promote', 'created', 'changed', 'timestamp', 'nid', 'uid', 'revision');
    }

    static function get_entity_type($entity) {
        return method_exists($entity, 'entityType') ? $entity->entityType() : $entity->type;
    }

    static function get_entity_language($entity) {
        return LANGUAGE_NONE;
        return method_exists($entity, 'langcode') ? $entity->langcode() : $entity->language;
    }

    static function save($entity) {
        return method_exists($entity, 'save') ? $entity->save() : node_save($entity);
    }

    static function set_deefault_values($entity, $field) {
        $items = field_get_default_value(self::get_entity_type($entity), $entity, $field, $instance);
        return $items;
    }
}

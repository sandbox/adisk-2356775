<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 20.09.14
 * Time: 19:41
 */

namespace Grabber;


class Mapper
{
    function execute($node_type, $grabber_name, $item) {
        $mapped = array();
        $map = $this->get_map($node_type, $grabber_name);

        // remap
        foreach($map as $from=>$to) {
            if (isset($item[$from])) {
                $mapped[$to] = $item[$from];
            }
        }

        return $mapped;
    }

    function get_map($node_type, $grabber_name) {
        $map = variable_get("grabber-map-{$node_type}-{$grabber_name}", array());

        if (!$map) {
            throw new \Exception("No field mapping for: '{$node_type}' - '{$grabber_name}'");
        }

        return $map;
    }
}



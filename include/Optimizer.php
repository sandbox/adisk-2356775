<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 08.10.14
 * Time: 12:27
 */

namespace Grabber;

require_once('Store.php');

class Optimizer {
    var $store = NULL;

    function __construct(Store $store) {
        $this->store = $store;
    }

    function execute(&$items) {
        $this->group($items, array('field_city', 'field_place', 'title'));
    }

    function group(array &$items, array $group_by) {
        $grouped = array();

        switch (count($group_by)) {
            case 1:
                list($g1) = $group_by;
                break;
            case 2:
                list($g1, $g2) = $group_by;
                break;
            case 3:
                list($g1, $g2, $g3) = $group_by;
                break;
            case 4:
                list($g1, $g2, $g3, $g4) = $group_by;
                break;
            case 5:
                list($g1, $g2, $g3, $g4, $g5) = $group_by;
                break;
            default:
                throw new \Exception("Group fields count must be from 1 to 5. Given: " . count($group_by));
        }

        foreach($items as $i=>&$item) {
            switch (count($group_by)) {
                case 1:
                    $k1 = $item['item'][$g1];

                    if (isset($grouped[$k1])) {
                        $existent =& $grouped[$k1];
                        $this->merge($existent, $item);
                        $grouped[$k1] =& $existent;

                        $this->store->backup($item);
                        $this->store->write($existent);
                        unset($items[$i]);
                    } else {
                        $grouped[$k1] = $item;
                    }
                    break;

                case 2:
                    $k1 = $item['item'][$g1];
                    $k2 = $item['item'][$g2];

                    if (isset($grouped[$k1][$k2])) {
                        $existent =& $grouped[$k1][$k2];
                        $this->merge($existent, $item);
                        $grouped[$k1][$k2] =& $existent;

                        $this->store->backup($item);
                        $this->store->write($existent);
                        unset($items[$i]);
                    } else {
                        $grouped[$k1][$k2] = $item;
                    }
                    break;

                case 3:
                    $k1 = $item['item'][$g1];
                    $k2 = $item['item'][$g2];
                    $k3 = $item['item'][$g3];

                    if (isset($grouped[$k1][$k2][$k3])) {
                        $existent =& $grouped[$k1][$k2][$k3];
                        $this->merge($existent, $item);
                        $grouped[$k1][$k2][$k3] =& $existent;

                        $this->store->backup($item);
                        $this->store->write($existent);
                        unset($items[$i]);
                    } else {
                        $grouped[$k1][$k2][$k3] = $item;
                    }
                    break;

                case 4:
                    $k1 = $item['item'][$g1];
                    $k2 = $item['item'][$g2];
                    $k3 = $item['item'][$g3];
                    $k4 = $item['item'][$g4];

                    if (isset($grouped[$k1][$k2][$k3][$k4])) {
                        $existent =& $grouped[$k1][$k2][$k3][$k4];
                        $this->merge($existent, $item);
                        $grouped[$k1][$k2][$k3][$k4] =& $existent;

                        $this->store->backup($item);
                        $this->store->write($existent);
                        unset($items[$i]);
                    } else {
                        $grouped[$k1][$k2][$k3][$k4] = $item;
                    }
                    break;

                case 5:
                    $k1 = $item['item'][$g1];
                    $k2 = $item['item'][$g2];
                    $k3 = $item['item'][$g3];
                    $k4 = $item['item'][$g4];
                    $k5 = $item['item'][$g5];

                    if (isset($grouped[$k1][$k2][$k3][$k4][$k5])) {
                        $existent =& $grouped[$k1][$k2][$k3][$k4][$k5];
                        $this->merge($existent, $item);
                        $grouped[$k1][$k2][$k3][$k4][$k5] =& $existent;

                        $this->store->backup($item);
                        $this->store->write($existent);
                        unset($items[$i]);
                    } else {
                        $grouped[$k1][$k2][$k3][$k4][$k5] = $item;
                    }
                    break;
            }
        }
    }

    function merge(&$item1, $item2) {
        foreach($item1['item'] as $key1=>$value1) {
            //$result['item'][$key1] = $value1;

            if (isset($item2['item'][$key1])) {
                $value2 = $item2['item'][$key1];

                if (empty($value2)) {
                    continue;
                }

                // overwrite empty value
                if (empty($value1)) {
                    $item1['item'][$key1] = $value2;
                    continue;
                };

                // merge taxonomy array
                if ( is_array($value1) && is_array($value2)) {
                    reset($value2);
                    $k = key($value2);
                    $v = current($value2);

                    // really taxonomy
                    if (is_numeric($k) && is_string($v)) {
                        // merge
                        $merged = array_merge($value1, $value2);
                        $uniqued = array_unique($merged);
                        $item1['item'][$key1] = $uniqued;
                        continue;
                    }
                }

                // merge field collection
                if ( is_array($value1) && is_array($value2) ) {
                    reset($value2);
                    $k = key($value2);
                    $fc_items = current($value2);

                    // really field collection
                    if (is_numeric($k) && is_array($fc_items)) {
                        // merge
                        $merged = array_merge($value1, $value2);
                        $uniqued = $this->unique_field_collection($merged);
                        $item1['item'][$key1] = $uniqued;
                        continue;
                    }
                }
            }
        }
    }

    function unique_field_collection($items) {
        $result = $items;
        return $result;
    }
}

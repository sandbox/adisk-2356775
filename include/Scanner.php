<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 20.09.14
 * Time: 21:17
 */

namespace Grabber;


/**
 * Class Grabber
 * @package Grabber
 */
class Scanner
{
    const EVENT_ITEM_PARSED = 'save';

    var $_handlers = array();

    /**
     * @return array
     */
    function execute() {
        return array();
    }

    /**
     * @param $event
     * @param $data
     */
    function emit($event, $data) {
        if (!isset($this->_handlers[$event]))
            return;

        foreach($this->_handlers[$event] as $handler) {
            // object or function
            if (is_array($handler)) {
                $instance = $handler[0];
                $method = $handler[1];
                $instance->$method($data);
            } else {
                $handler($data);
            }

        }
    }

    function fetch_images($item, $image_fields) {
        //
    }

    /**
     * @param $event
     * @param $handler
     * @return $this
     */
    function on($event, $handler) {
        $this->_handlers[$event][] = $handler;
        return $this;
    }

    /**
     * @param $item
     */
    function emit_item_parsed($item, $image_fields=array()) {
        $this->emit(self::EVENT_ITEM_PARSED, array('item' => $item, 'grabber' => $this, '$image_fields' => $image_fields));
    }
}


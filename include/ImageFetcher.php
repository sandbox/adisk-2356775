<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 12.09.14
 * Time: 20:18
 */

namespace Grabber;


/**
 * Class ImageFetcher
 * @package Grabber
 */
class ImageFetcher {
    /**
     * Fetch image to local folder
     * @param $url
     * @param Store $store
     * @param $filename
     * @return mixed
     */
    function execute($url, Store $store, $filename) {
        $content = file_get_contents($url);
        $fileout = $store->write_image($filename, $content);
        return $fileout;
    }
}

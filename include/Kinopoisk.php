<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 17.09.14
 * Time: 17:22
 */

namespace Grabber;

require_once(__DIR__ . '/simple_html_dom.inc.php');


class Kinopoisk {
    function execute($title, $casts, $year) {
        $url = $this->search($title, $casts, $year);
        $item = $this->parse($url);

        return $item;
    }

    function search($title, $casts, $year) {
        $headers =
            "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\n".
            "Accept-Encoding:gzip,deflate,sdch\r\n".
            "Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,fr;q=0.2\r\n".
            "Connection:keep-alive\r\n".
            //"Cookie:PHPSESSID=a64df3d148a33159bf5bfeb123b608f6; mobile=no; tc=3001; awfs=1; last_visit=2014-09-17+14%3A50%3A16; noflash=false; result_type=simple; user_country=ru; my_perpages=%5B%5D; users_info[check_sh_bool]=none; _ym_visorc_22663942=b\r\n".
            "Host:www.kinopoisk.ru\r\n".
            "Referer:http://www.kinopoisk.ru/s/\r\n".
            "User-Agent:Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36\r\n";

        $request = 'http://www.kinopoisk.ru/index.php?' . http_build_query(
                array(
                    'level' => 7,
                    'from' => 'forma',
                    'result' => 'adv',
                    'm_act[from]' => 'forma',
                    'm_act[what]' => 'content',
                    'm_act[find]' => $title,
                    'm_act[cast]' => $casts,
                )
            );

        $opts = array(
            'http'=>array(
                'method' => "GET",
                'header' => $headers
            )
        );

        $context = stream_context_create($opts);

        $html = file_get_html($request, false, $context);

        // Find all items
        $results = $html->find('.search_results', 0);

        // Get url of first
        $url = $results->find('.element .info .name a', 0)->href;

        return $url;
    }

    function parse($url) {
        $item = array();

        // http://www.kinopoisk.ru/film/771227/
        $html = file_get_html($url);

        $cells = $html->find('#infoTable tr td.type');

        foreach($cells as $cell) {
            switch ($cell->plaintext) {
                case 'год':
                    $item['year'] = $cell->next_sibling()->plaintext;
                    break;

                case 'страна':
                    $item['country'] = $cell->next_sibling()->plaintext;
                    break;

                case 'слоган':
                    $item['slogan'] = $cell->next_sibling()->plaintext;
                    break;

                case 'режиссер':
                    $item['regisser'] = $cell->next_sibling()->plaintext;
                    break;

                case 'сценарий':
                    $item['scenary'] = $cell->next_sibling()->plaintext;
                    break;

                case 'продюсер':
                    $item['produsser'] = $cell->next_sibling()->plaintext;
                    break;

                case 'оператор':
                    $item['operator'] = $cell->next_sibling()->plaintext;
                    break;

                case 'композитор':
                    $item['compositor'] = $cell->next_sibling()->plaintext;
                    break;

                case 'художник':
                    $item['hudoznik'] = $cell->next_sibling()->plaintext;
                    break;

                case 'монтаж':
                    $item['monta'] = $cell->next_sibling()->plaintext;
                    break;

                case 'жанр':
                    $ganr = $cell->next_sibling()->plaintext;
                    $ganr = explode(',', $ganr);
                    $ganr = array_map('trim', $ganr);
                    $ganr = array_map('strtolower', $ganr);
                    $item['field_ganr'] = $ganr;
                    break;

                case 'бюджет':
                    $item['budget'] = $cell->next_sibling()->plaintext;
                    break;

                case 'сборы в США':
                    $item['cash_usa'] = $cell->next_sibling()->plaintext;
                    break;

                case 'премьера (мир)':
                    $item['cash_world'] = $cell->next_sibling()->plaintext;
                    break;

                case 'премьера (РФ)':
                    $item['cash_russia'] = $cell->next_sibling()->plaintext;
                    break;

                case 'релиз на DVD':
                    $item['release_dvd'] = $cell->next_sibling()->plaintext;
                    break;

                case 'релиз на Blu-Ray':
                    $item['release_blueray'] = $cell->next_sibling()->plaintext;
                    break;

                case 'возраст':
                    $item['age'] = $cell->next_sibling()->plaintext;
                    break;

                case 'рейтинг MPAA':
                    $item['rank_MPAA'] = $cell->next_sibling()->plaintext;
                    break;

                case 'время':
                    $item['time'] = $cell->next_sibling()->plaintext;
                    break;

            }
        }

        return $item;
    }
}

$k = new Kinopoisk();
$item = $k->execute('История дельфина 2', 'Чарльз Мартин Смит', 2014);
print_r($item);


<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 12.09.14
 * Time: 19:19
 */
namespace Grabber;

require_once('ImageFetcher.php');
require_once('Store.php');
require_once('Afisha.php');
require_once('Scanner.php');
require_once('Cache.php');
require_once('Logger.php');


/**
 * Class Coordinator
 * Main grabber class
 * @package Grabber
 */
class Coordinator
{
    /**
     * Execute grabbing
     */
    function execute() {
        $grabbers = $this->get_grabbers();

        foreach($grabbers as $grabber) {
            \Grabber\Logger::log('Grabber ' . get_class($grabber) . ' start.');
            $grabber->on(Scanner::EVENT_ITEM_PARSED, array($this, 'on_item_parsed'));
            $grabber->execute();
            \Grabber\Logger::log('Grabber ' . get_class($grabber) . ' end.');
        }
    }

    /**
     * Return grabbers
     * Search grabbers in subfolder. Load. Create instances,
     * @return array
     */
    function get_grabbers() {
        $dir = __DIR__ . '/../scanners';
        $matches = array();
        $grabbers = array();

        $files = scandir($dir);

        foreach($files as $file) {
            if ($file == '..' || $file =='.') continue;
            if (!preg_match('/\.php$/', $file, $matches)) continue;

            include_once($dir . '/' . $file);
            $class = str_replace(".php", "", $file);
            $class = ucfirst($class);
            $class = '\\Grabber\\' . $class;
            $grabbers[] = new $class();
        }

        return $grabbers;
    }

    /**
     * Callback. Emitted by Grabber. When item parsed.
     * @param array $event
     */
    function on_item_parsed(array $event) {
        $item = $event['item'];

        $grabber = $event['grabber'];
        //$image_fields = $event['image_fields'];
        $grabber_class = get_class($grabber);
        $exploded = explode('\\', $grabber_class);
        $grabber_class = array_pop($exploded);
        //$item['grabber'] = $grabber_class;

        // clean
        foreach($item as $field=>&$value) {
            if (is_string($value)) {
                $value = trim($value);
            }
        }

        // fill required. from afisha.ru
        $this->fill_empty_fields($item);

        // save
        $store = new Store();
        $write_item = array(
            'grabber' => $grabber_class,
            'item' => $item,
        );
        $store->write($write_item);

        // fetch images
        //$imaged = array_combine($image_fields, $image_fields);
        //$urls = array_intersect_key($item, $imaged);

        //$fetched = $this->fetch_images($urls, $store);

        //$store->write('fetched.json', json_encode($fetched));
        Logger::log('Parsed ' . $item['title']);
    }

    /**
     * Fetch images.
     * Save to local folder.
     * @param array $urls
     * @param Store $store
     * @return array
     */
    function fetch_images(array $urls, Store $store) {
        $fetched = array();

        $fetcher = new ImageFetcher();

        foreach($urls as $field=>$url) {
            if (is_array($url)) {
                $array = $url;
                foreach($array[$field] as $i=>$url) {
                    $ext = pathinfo($url, PATHINFO_EXTENSION);
                    $filename = $field . '/' . $i . '.' . $ext;
                    $local = $fetcher->execute($url, $store, $filename);
                    $fetched[$field][$i] = $local;
                }
            } else {
                $ext = pathinfo($url, PATHINFO_EXTENSION);
                $filename = $field . '/' . '0' . '.' . $ext;
                $local = $fetcher->execute($url, $store, $filename);
                $fetched[$field] = $local;
            }
        }

        return $fetched;
    }

    function fill_empty_fields(&$item) {
        $required_fields = array(
            // Основная информация
            //'title' => 'test',

            // group_leftgroup
            //'field_city'            => 'Магнитогорск', // Ссылка на термин // Магнитогорск
            //'field_categoryhappy'   => 'Кино',  // Ссылка на термин // Кино
            //'field_place'           => 'Night club «Piramida»', // Ссылка на термин //
            //'field_kadrs'           => '',  // Изображение

            // Right group
            //'field_blockslider'     => '', // Флажки/переключатели
            //'field_datapremiere'    => '', // Дата
            //'field_imgslider'       => '', // Изображение
            //'field_bgpost'          => '', // Изображение
            'field_age'             => '', // Ссылка на термин
            'field_media'           => '', // Файл
            'field_poster'          => '', // Изображение

            // Информация о фильме
            // leftfilm
            //'field_ff'              => '', // Ссылка на термин
            'field_year'            => '', // Ссылка на термин
            'field_time'            => '', // Текст
            'field_regiser'         => '', // Текст

            // rightfilm
            'field_country'         => '', // Ссылка на термин
            'field_ganr'            => '', // Ссылка на термин
            'field_roles'           => '', // Текст
            //'field_rating'          => '', // Длинный текст

            'field_context'         => 'text', // Полный текст с анонсом

            // Расписание
            //'field_tabletime_sum'   => array(
            //    0 => array('field_tabletime' => '13:00', 'field_priceticket' => '100'),
            //    1 => array('field_tabletime' => '14:00', 'field_priceticket' => '100'),
            //    2 => array('field_tabletime' => '15:00', 'field_priceticket' => '100'),
            //),
        );

        // request to afisha.ru
        try {
            $cached = Cache::get('Afisha-' . $item['title']);

            if (!$cached) {
                $afisha = new Afisha();
                $data = $afisha->execute($item['title'], '', '');

                Cache::set('Afisha-' . $item['title'], $data);
            }

            foreach($required_fields as $field=>$stub) {
                if (isset($item[$field]) && $item[$field] !== '') {
                    continue;
                }

                if (!isset($data[$field]))
                    continue;

                $item[$field] = $data[$field];
            }
        } catch (\Exception $e) {
            Logger::error("Not fount in afisha: " . $item['title']);
            // TODO check another kinosite
        }
    }
}

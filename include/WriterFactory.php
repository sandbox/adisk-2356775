<?php
/**
 * Created by PhpStorm.
 * User: vital
 * Date: 20.09.14
 * Time: 19:45
 */

namespace Grabber;

require_once('Cache.php');
require_once('NodeHelper.php');
require_once('TaxonomyHelper.php');
require_once('Settings.php');
require_once('Logger.php');


class WriterFactory
{
    static function get($entity_type, $bundle_name, $field_name) {
        $cache_key = "writer-{$entity_type}-{$bundle_name}-{$field_name}";

        $writer = Cache::get($cache_key);

        if ($writer) {
            return $writer;
        }

        $info = NodeHelper::get_node_info($entity_type, $bundle_name);

        $field_info = $info[$field_name]['field_info'];
        $field_type = $field_info['type'];
        $settings = $info[$field_name];

        $map = array(
            'datetime' =>                   '\Grabber\DateFieldWriter',
            'date' =>                       '\Grabber\DateFieldWriter',
            'taxonomy_term_reference' =>    '\Grabber\TaxonomyFieldWriter',
            'field_collection' =>           '\Grabber\FieldCollectionFieldWriter',
            'number_integer' =>             '\Grabber\TextFieldWriter',
            'text' =>                       '\Grabber\TextFieldWriter',
            'text_long' =>                  '\Grabber\TextFieldWriter',
            'text_with_summary' =>          '\Grabber\TextFieldWriter',
            'image' =>                      '\Grabber\ImageFieldWriter',
            'media' =>                      '\Grabber\MediaFieldWriter',
            'file'  =>                      '\Grabber\FileFieldWriter',
        );

        if (!isset($map[$field_type])) {
            throw new \Exception("No writer for: '{$field_type}'");
        }

        $class = $map[$field_type];

        $writer = new $class($field_name, $settings);

        Cache::set($cache_key, $writer);

        return $writer;
    }
}



class FieldWriter
{
    var $field_name = '';
    var $settings = array();

    function __construct($field_name, array $settings) {
        $this->field_name = $field_name;
        $this->settings = $settings;
    }

    function execute(&$entity, $value) {
        //
    }

    function validate($value) {
        return TRUE;
    }
}


class TaxonomyFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        $terms = is_array($value) ? $value : array($value);
        $voc_name = $this->settings['field_info']['settings']['allowed_values'][0]['vocabulary'];
        $language = NodeHelper::get_entity_language($entity);
        $taxonomy_helper = new TaxonomyHelper();

        foreach($terms as $i=>$term) {
            $tid = $taxonomy_helper->get_term($voc_name, $term, TRUE);
            $entity->{$this->field_name}[$language][$i]['tid'] = $tid;
        }
    }
}


class FieldCollectionFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        if ($entity && $this->field_name && !empty($value)) {
            $language = NodeHelper::get_entity_language($entity);

            // merge, unique. if need
            $this->prepare($entity, $value);

            // delete old values
            $fc_items = field_get_items('node', $entity, $this->field_name);

            if ($fc_items) {
                $ids = field_collection_field_item_to_ids($fc_items);
                entity_delete_multiple('field_collection_item', $ids);

                unset($entity->{$this->field_name});
            }

            // set new values
            // multi valued field collection support
            foreach($value as $delta=>$subvalues) {
                $fc = entity_create('field_collection_item', array('field_name' => $this->field_name));
                $fc->setHostEntity('node', $entity, $language);

                // set each field
                foreach($subvalues as $subfield_name=>$multivalued) {
                    $writer = WriterFactory::get('field_collection_item', $this->field_name, $subfield_name);
                    $writer->execute($fc, $multivalued);
                }

                $fc->save($skip_host_save=TRUE);

                $entity->{$this->field_name}[$language][$delta]['value'] = $fc->item_id;
                //if (!empty($entity->revision)) {
                //    $entity->{$this->field_name}[$language][$delta]['revision_id'] = $entity->revision;
                //}
            }
        }
    }

    function get_field_collection_fields() {
        $fields = field_info_instances('field_collection_item', $this->field_name);

        foreach($fields as $field_name=>&$info) {
            $info['field_info'] = field_info_field($field_name);
        }

        return $fields;
    }

    function get_settings($entity_type) {
        $settings = Settings::grabber_get_type_settings($entity_type);
        return $settings[$this->field_name];
    }

    function prepare(&$entity, array &$value) {
        // 1. load
        // 2. index
        // 3. add new, update old
        // 4. generate new collection

        $entity_type = NodeHelper::get_entity_type($entity);
        $settings = $this->get_settings($entity_type);

        // update
        if ($settings['merge']) {
            $merged = array();

            // load
            $fc_items = field_get_items('node', $entity, $this->field_name);

            if ($fc_items) {
                $fc_info = $this->get_field_collection_fields();

                $ids = field_collection_field_item_to_ids($fc_items);
                $fc_entities = entity_load('field_collection_item', $ids);

                foreach ($fc_entities as $fc) {
                    // skip empty
                    if ($fc === FALSE) {
                        continue;
                    }

                    $merge_row = array();

                    // each field of field collection
                    foreach($fc_info as $fc_field=>$fc_field_info) {
                        $fc_items = field_get_items('field_collection_item', $fc, $fc_field);
                        $fc_property = NodeHelper::get_central_property($fc_info, $fc_field);
                        $merge_row[$fc_field] = $fc_items[0][$fc_property];
                    }

                    $merged[] = $merge_row;
                }
            }

            // append from item
            if (isset($value)) {
                foreach($value as $v) {
                    $merged[] = $v;
                }
            }

            // store to item
            $value = $merged;
        }

        if ($settings['unique']) {
            $indexed = array();
            $unique_field = $settings['unique_field'];
            $fc_info = $this->get_field_collection_fields();

            foreach($value as $v) {
                $key = $this->get_unique_key($fc_info, $unique_field, $v[$unique_field]);
                $indexed[$key] = $v;
            }

            // sort
            ksort($indexed);

            // generate new collection
            $value = array_values($indexed);
        }
    }

    function get_unique_key($fc_info, $fc_field, $value) {
        $type = $fc_info[$fc_field]['field_info']['type'];

        switch ($type) {
            case 'date':
            case 'datetime':
                $key = (new \DateTime($value))->getTimestamp();
                break;

            default:
                $key = $value;
        }

        return $key;
    }
}


class DateFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        $values = is_array($value) ? $value : array($value);
        $language = NodeHelper::get_entity_language($entity);

        foreach($values as $i=>$concrete_value) {
            try {
                $this->validate($concrete_value);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }

            $entity->{$this->field_name}[$language][$i]['value'] = $concrete_value;
        }
    }
}


class TextFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        $values = is_array($value) ? $value : array($value);
        $language = NodeHelper::get_entity_language($entity);

        foreach($values as $i=>$concrete_value) {
            try {
                $this->validate($concrete_value);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }

            $entity->{$this->field_name}[$language][$i]['value'] = $concrete_value;
        }
    }
}


class OptionsFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        $values = is_array($value) ? $value : array($value);
        $language = NodeHelper::get_entity_language($entity);

        foreach($values as $i=>$concrete_value) {
            try {
                $this->validate($concrete_value);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }

            $entity->{$this->field_name}[$language][$i]['value'] = $concrete_value;
        }
    }
}


class ImageFieldWriter extends FieldWriter
{
    function validate($value) {
        if (!file_exists($value)) {
            throw new \Exception("warn: File not exists: {$value}");
        }
    }

    function execute(&$entity, $value) {
        global $user;

        $values = is_array($value) ? $value : array($value);
        $language = NodeHelper::get_entity_language($entity);

        foreach($values as $delta=>$concrete_value) {
            try {
                $this->validate($concrete_value);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }

            $file_path = drupal_realpath($concrete_value); // Create a File object
            $file = (object) array(
                'uid' => $user->uid,
                'uri' => $file_path,
                'filemime' => file_get_mimetype($file_path),
                'status' => 1,
            );
            $file = file_copy($file, 'public://');

            $entity->{$this->field_name}[$language][$delta] = (array)$file;
        }
    }
}


class MediaFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        global $user;

        $values = is_array($value) ? $value : array($value);
        $language = NodeHelper::get_entity_language($entity);

        foreach($values as $i=>$concrete_value) {
            try {
                $this->validate($concrete_value);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }

            $file = $this->file_load_by_uri($concrete_value);

            // not exist -> create
            if ($file) {
                $file->display = 1;
            } else {
                $file = (object) array(
                    'uid' => $user->uid,
                    'uri' => $concrete_value,
                    'filemime' => file_get_mimetype($concrete_value),
                    'status' => 1,
                );;
                $file->display = 1;
                $file->description = '';
                $file->timestamp = REQUEST_TIME;
                drupal_write_record('file_managed', $file);
                //file_save($file);
            }


            $entity->{$this->field_name}[$language][$i] = (array)$file;
        }
    }

    function file_load_by_uri($uri) {
        $uri = file_stream_wrapper_uri_normalize($uri);
        $files = entity_load('file', FALSE, array('uri' => $uri));
        return !empty($files) ? reset($files) : FALSE;
    }
}

class FileFieldWriter extends FieldWriter
{
    function execute(&$entity, $value) {
        global $user;

        $values = is_array($value) ? $value : array($value);
        $language = NodeHelper::get_entity_language($entity);

        foreach($values as $i=>$concrete_value) {
            try {
                $this->validate($concrete_value);
            } catch (\Exception $e) {
                Logger::error($e->getMessage());
                continue;
            }

            $file = $this->file_load_by_uri($concrete_value);

            // not exist -> create
            if ($file) {
                $file->display = 1;
            } else {
                $file = (object) array(
                    'uid' => $user->uid,
                    'uri' => $concrete_value,
                    'filemime' => file_get_mimetype($concrete_value),
                    'status' => 1,
                );
                $file->display = 1;
                $file->description = '';
                $file->timestamp = REQUEST_TIME;
                drupal_write_record('file_managed', $file);
                //file_save($file);
            }


            $entity->{$this->field_name}[$language][$i] = (array)$file;
        }
    }

    function file_load_by_uri($uri) {
        $uri = file_stream_wrapper_uri_normalize($uri);
        $files = entity_load('file', FALSE, array('uri' => $uri));
        return !empty($files) ? reset($files) : FALSE;
    }
}

// TODO if multivalued -> append/overwrite, single -> overwrite
// TODO append/overwrite as flag

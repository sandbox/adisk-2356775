<?php

namespace Grabber;

require_once(__DIR__ . '/../include/Scanner.php');
require_once(__DIR__ . '/../include/simple_html_dom.inc.php');


/**
 * Class Jazzcinema
 * @package Grabber
 */
class Jazzcinema extends Scanner
{
    function execute() {
        // Create DOM from URL or file
        $html = file_get_html('http://jazzcinema.ru');
        //$html = file_get_html('/home/vital/src/grabber/jazzcinema.ru/jazzcinema.ru/schedule/index.html');

        // Find all items
        $days = $html->find('.content .schedule');

        foreach($days as $day) {

            // @var $event simple_html_dom_node
            foreach($day->find('li.cf') as $event) {
                // Название, жанр
                $movie = $event->find('.movie', 0);
                $item = array();

                // hardcoded
                $item['node_type'] = 'happy';
                $item['field_city'] = 'Магнитогорск';
                $item['field_place'] = 'КТ «Jazz Cinema»';
                $item['field_categoryhappy'] = 'Кино';

                // title
                $title = $movie->find('.title', 0)->plaintext;

                // fix title
                if (preg_match('/(.*)\((\d+\+)\)\s*$/', $title, $matches) ) {
                    $item['title'] = $matches[1];
                } else {
                    $item['title'] = $title;
                }

                // age
                $matches = array();
                if (preg_match('/.*\((\d+\+)\)\s*$/', $title, $matches) ) {
                    $item['field_age'] = $matches[1];
                } else {
                    $item['field_age'] = '';
                };

                // Время, цена
                $seanses = $event->find('.seanses li');

                // data
                $date = $day->rel; // calendar-2014-09-15-schedule
                $date = str_replace('calendar-', '' , $date); // 2014-09-15-schedule
                $date = str_replace('-schedule', '' , $date); // 2014-09-15

                foreach($seanses as $seanse) {
                    // date + time
                    $datetime = $date . ' ' . $seanse->find('a', 0)->plaintext;

                    // digits only
                    $cost = $seanse->find('.price', 0)->plaintext;
                    $cost = filter_var($cost, FILTER_SANITIZE_NUMBER_INT);

                    $item['field_tabletime_sum'][] = array(
                        'field_tabletime' => $datetime,
                        'field_priceticket' =>  $cost,
                    );
                }

                // Видео
                $info = $event->find('.movie-info', 0);
                $video_script = $info->find('.video script', 0);
                $video_script = isset($video_script->innertext) ? $video_script->innertext : '';

                $matches = array();
                $trailer = '';

                //config = {'file':'/upload/trailers/pochtaljon_pat.mp4','height':'300','width':'400','dock':true,'id':'bx_flv_player_576726282','controlbar':'bottom','players':[{'type':'html5'},{'type':'flash','src':'/bitrix/components/bitrix/player/mediaplayer/player'}],'logo.hide':'true','repeat':'N','abouttext':'1С-Битрикс: Медиа-плеер','aboutlink':'http://www.1c-bitrix.ru/products/cms/features/mediaplayer.php'};
                if (preg_match('/config = {.*mediaplayer.php\'};/', $video_script, $matches)) {
                    $json = str_replace('config = ', '', $matches[0]);
                    $json = substr($json, 0, strlen($json)-1);
                    $json = str_replace('\'', '"', $json);
                    $parsed = json_decode($json);
                    $trailer = 'http://jazzcinema.ru' . $parsed->file;
                }

                $item['field_media'] = $trailer; // unsupported

                $poster = $info->find('.poster img', 0)->src;
                $poster = (stristr($poster, 'http://') === FALSE) ? 'http://jazzcinema.ru' . $poster : $poster;
                $item['field_poster'] = $poster;

                $advanced = $info->find('.mr table tr td');

                foreach($advanced as $adv) {
                    $content = $adv->innertext;

                    switch ($content) {
                        case 'Начало проката:':
                            $field_datapremiere = $adv->next_sibling()->plaintext;
                            $date = date_parse_from_format ("d.m.Y", $field_datapremiere);
                            $item['field_datapremiere'] = $date['year'] . '-' . $date['month'] . '-' . $date['day'] ;
                            break;

                        case '<strong>Окончание проката:</strong>':
                            //$item['show_end'] = $adv->next_sibling()->plaintext;
                            break;

                        case 'Жанр:':
                            $ganr = $adv->next_sibling()->plaintext;
                            $ganr = explode(',', $ganr);
                            $ganr = array_map('trim', $ganr);
                            $ganr = array_map('strtolower', $ganr);
                            $item['field_ganr'] = $ganr;
                            break;

                        case 'Режиссёр:':
                            $item['field_regiser'] = $adv->next_sibling()->plaintext;
                            break;

                        case 'В ролях:':
                            $item['field_roles'] = $adv->next_sibling()->plaintext;
                            break;

                        case 'Аннотация:':
                            $item['field_context'] = $adv->next_sibling()->plaintext;
                            break;

                        case 'Продолжительность:':
                            $item['field_time'] = $adv->next_sibling()->plaintext;
                            break;

                    }
                }

                //$this->fetch_images($item, $image_fields=array('field_kadrs', 'field_imgslider', 'field_bgpost', 'field_poster'));
                $this->emit_item_parsed($item);
            }
        }

    }
}
